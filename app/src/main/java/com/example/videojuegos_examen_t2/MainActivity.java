package com.example.videojuegos_examen_t2;

import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.videojuegos_examen_t2.adapters.ContactAdapter;
import com.example.videojuegos_examen_t2.entities.Contact;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        List<Contact> contacts = getContacts();
        ContactAdapter adapter = new ContactAdapter(contacts);

        RecyclerView rv = findViewById(R.id.rvContacts);
        rv.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        rv.setHasFixedSize(true);
        rv.setAdapter(adapter);



        Button botonLlamar = findViewById(R.id.llamada);
        Button botonMensajear = findViewById(R.id.Mensaje);

        botonLlamar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:+51901325139"));
                if(ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CALL_PHONE)!=
                        PackageManager.PERMISSION_GRANTED)
                    return;
                startActivity(intent);
            }
        });

        botonMensajear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:+51901325139"));
                if(ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.SEND_SMS)!=
                        PackageManager.PERMISSION_GRANTED)
                    return;
                startActivity(intent);
            }
        });
    }

    private List<Contact> getContacts() {
        List<Contact> contacts = new ArrayList<>();
        contacts.add(new Contact(1,"Pablo Burgos","542-590-6572"));
        contacts.add(new Contact(2,"Leonars Villanueva","+51901325139"));
        contacts.add(new Contact(3,"Jheferson Mendoza","435-440-2851"));
        contacts.add(new Contact(4,"Leonidas Villanueva","572-705-6101"));
        contacts.add(new Contact(5,"Edith Mendoza","543-296-9061"));
        contacts.add(new Contact(6,"Nancy Mendoza","267-342-8794"));
        contacts.add(new Contact(7,"Brayan Vasquez","778-359-4226"));
        contacts.add(new Contact(8,"Jorge Vasquez","597-338-7011"));
        contacts.add(new Contact(9,"Alberto Villanueva","932-661-1183"));
        contacts.add(new Contact(10,"Celeste Naomi","563-845-9088"));

        return contacts;
    }
}